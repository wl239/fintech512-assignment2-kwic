import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

class KwicTest {
    @Test
    @Disabled("Test Ignored Words")
    void testIgnored() {
        String input = "is\n" + "the\n" + "of\n" + "and\n" + "as\n" +
                "a\n" + "but\n" + "::\n" + "Descent of Man\n"
                + "The Ascent of Man\n" +
                "The Old Man and The Sea\n" +
                "A Portrait of The Artist As a Young Man\n" +
                "A Man is a Man but Bubblesort IS A DOG\n";
        String expected = "is\n" + "the\n" + "of\n" +
                "and\n" + "as\n" + "a\n" + "but\n";
        assertEquals(new Kwic().get_ignored_words(input), new Kwic().get_array_list(expected));
    }

    @Test
    @Disabled("Test Titles")
    void testTitles() {
        String input = "is\n" + "the\n" + "of\n" + "and\n" + "as\n" +
                "a\n" + "but\n" + "::\n" + "Descent of Man\n"
                + "The Ascent of Man\n" +
                "The Old Man and The Sea\n" +
                "A Portrait of The Artist As a Young Man\n" +
                "A Man is a Man but Bubblesort IS A DOG\n";
        String expected = "Descent of Man\n" +
                "The Ascent of Man\n" +
                "The Old Man and The Sea\n" +
                "A Portrait of The Artist As a Young Man\n" +
                "A Man is a Man but Bubblesort IS A DOG\n";
        assertEquals(new Kwic().get_titles(input), new Kwic().get_array_list(expected));
    }

    @Test
    @Disabled("Test Keywords")
    void testKeywords(){
        String input = "is\n" + "the\n" + "of\n" + "and\n" + "as\n" +
                "a\n" + "but\n" + "::\n" + "Descent of Man\n"
                + "The Ascent of Man\n" +
                "The Old Man and The Sea\n" +
                "A Portrait of The Artist As a Young Man\n" +
                "A Man is a Man but Bubblesort IS A DOG\n";
        String expected = "Descent\n" + "Man\n" + "Ascent\n" + "Old\n" +
                "Sea\n" + "Portrait\n" + "Artist\n" + "Young\n" + "Bubblesort\n" + "Dog\n";
        assertEquals(new Kwic().get_keywords(input), new Kwic().get_array_list(expected));
    }

    @Test
    @Disabled("Test Output")
    void testOutput() {
        String input = "is\n" + "the\n" + "of\n" + "and\n" + "as\n" +
                "a\n" + "but\n" + "::\n" + "Descent of Man\n"
                + "The Ascent of Man\n" +
                "The Old Man and The Sea\n" +
                "A Portrait of The Artist As a Young Man\n" +
                "A Man is a Man but Bubblesort IS A DOG\n";

        String expected = "a portrait of the ARTIST as a young man\n" +
                "the ASCENT of man\n" +
                "a man is a man but BUBBLESORT is a dog\n" +
                "DESCENT of man\n" +
                "a man is a man but bubblesort is a DOG\n" +
                "descent of MAN\n" +
                "the ascent of MAN\n" +
                "the old MAN and the sea\n" +
                "a portrait of the artist as a young MAN\n" +
                "a MAN is a man but bubblesort is a dog\n" +
                "a man is a MAN but bubblesort is a dog\n" +
                "the OLD man and the sea\n" +
                "a PORTRAIT of the artist as a young man\n" +
                "the old man and the SEA\n" +
                "a portrait of the artist as a YOUNG man\n";

        assertEquals(new Kwic().get_kwic_output(input), expected);
    }
}