import java.util.ArrayList;
import java.util.*;

import static java.util.Collections.frequency;

class Kwic {
    public ArrayList<String> get_ignored_words(String input) {
        String[] split_input = input.split("::");
        String ignoredStr = split_input[0];
        return get_array_list(ignoredStr);
    }

    public ArrayList<String> get_titles(String input) {
        String[] split_input = input.split("::\n");
        String titleStr = split_input[1];
        return get_array_list(titleStr);
    }

    public ArrayList<String> get_array_list(String str) {
        String[] split_str = str.toLowerCase().split("\n");
        return new ArrayList<>(Arrays.asList(split_str));
    }

    public ArrayList<String> get_keywords(String input) {
        ArrayList<String> ignoredList = get_ignored_words(input);
        ArrayList<String> titles = get_titles(input);

        ArrayList<String> keywords = new ArrayList<>();

        for (String title : titles) {
            String[] words = title.split(" ");
            for (String word : words) {
                if (!ignoredList.contains(word)) {
                    keywords.add(word);
                    ignoredList.add(word);
                }
            }
        }
        return keywords;
    }

    public String get_kwic_output(String input) {
        ArrayList<String> keywords = get_keywords(input);
        keywords.sort(Comparator.naturalOrder());
        ArrayList<String> titlelist = get_titles(input);

        StringBuilder output = new StringBuilder();
        for (String keyword : keywords) {
            for (String s : titlelist) {
                ArrayList<String> words = new ArrayList<>(Arrays.asList(s.split(" ")));
                int count = frequency(words, keyword);
                if (count != 0) {
                    int index = words.indexOf(keyword);
                    if (count == 1) {
                        words.set(index, keyword.toUpperCase());
                        String convert_str = String.join(" ", words);
                        output.append(convert_str);
                        output.append("\n");
                    }
                    else {
                        int last_index = words.lastIndexOf(keyword);
                        for (int k = index; k <= last_index; k++) {
                            if (Objects.equals(words.get(k), keyword)) {
                                words.set(k, keyword.toUpperCase());
                                String convert_str = String.join(" ", words);
                                output.append(convert_str);
                                output.append("\n");
                                words.set(k, keyword.toLowerCase());
                            }
                        }
                    }
                }
            }
        }
        return output.toString();
    }
}